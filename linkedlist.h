#ifndef LINKEDLIST_H
#define LINKEDLIST_H

/*
 * Implementation of a singly linked list
 */

struct linked_list_node 
{
    void *data;
    struct linked_list_node *next;
};

struct linked_list_node* linked_list_kth_node_from_end(struct linked_list_node *node, int k);

struct linked_list_node* linked_list_reverse(struct linked_list_node *node);

int linked_list_has_loop(struct linked_list_node *node);

struct linked_list_node* linked_list_add_list_numbers(struct linked_list_node *la
                                                    , struct linked_list_node *lb);

#endif
