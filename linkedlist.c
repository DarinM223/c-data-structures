#include "linkedlist.h"
#include <stdlib.h>
#include <stdio.h>

/*
 * Returns the kth node of a linked list
 */
struct linked_list_node* linked_list_kth_node_from_end(struct linked_list_node *node, int k) 
{
    if (k == 0 || node == NULL)
        return NULL;

    int size = 0;
    struct linked_list_node *ctr = node;

    /* pass through list once to get size */
    while (ctr != NULL) { 
        ++size;
        ctr = ctr->next;
    }

    int diff = size + 1 - k;
    diff = (diff >= 1 ? diff : 1); 

    ctr = node; 
    while (ctr != NULL) {
        --diff;
        if (diff == 0) {
            return ctr;
        }
        ctr = ctr->next;
    }

    return NULL;
}

/*
 * Reverses a linked list and returns the HEAD of the new reversed list
 */
struct linked_list_node* linked_list_reverse(struct linked_list_node *node) 
{
    struct linked_list_node *reversed_node = NULL, *curr = node, *prev = NULL;

    while (curr != NULL) {
        struct linked_list_node *next = curr->next;

        /* if at the end, set the HEAD of the reversed list */
        if (next == NULL) {
            reversed_node = curr;
        }

        /* Change the next pointers to point to the previous node instead of the next node */
        curr->next = prev;

        prev = curr;
        curr = next;
    }    
    return reversed_node;
}

/*
 * Returns 1 if a linked list has a loop, 0 if there is no loop
 */
int linked_list_has_loop(struct linked_list_node *node) 
{
    /* Create two nodes, one that jumps two nodes every turn and one that jumps one node every turn */
    struct linked_list_node *slower_node = node, *faster_node = node;

    /* If the two nodes ever meet and none of them become NULL, then there is a loop */
    do {
        if (slower_node->next == NULL) return 0;
        slower_node = slower_node->next;

        if (faster_node->next == NULL || faster_node->next->next == NULL) return 0;
        faster_node = faster_node->next->next;
    } while (slower_node != faster_node);

    return 1;
}
