#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "linkedlist.h"

void test_kth_node_from_end() 
{
    int v1 = 10, v2 = 15, v3 = 20, v4 = 30;
    struct linked_list_node *node  = malloc(sizeof(*node));
    struct linked_list_node *node2 = malloc(sizeof(*node2));
    struct linked_list_node *node3 = malloc(sizeof(*node3));
    struct linked_list_node *node4 = malloc(sizeof(*node4));

    node->data = &v1;
    node2->data = &v2;
    node3->data = &v3;
    node4->data = &v4;

    node->next = node2;
    node2->next = node3;
    node3->next = node4;
    node4->next = NULL;

    struct linked_list_node *kth1 = linked_list_kth_node_from_end(node, 2);
    struct linked_list_node *kth2 = linked_list_kth_node_from_end(node, 1);
    struct linked_list_node *kth3 = linked_list_kth_node_from_end(node, 0);
    struct linked_list_node *kth4 = linked_list_kth_node_from_end(node, 10);
    assert(*((int*)kth1->data) == 20);
    assert(*((int*)kth2->data) == 30);
    assert(kth3 == NULL);
    assert(*((int*)kth4->data) == 10); /* if k > list size default to first element */
}

void test_reverse() 
{
    int v1 = 1, v2 = 2, v3 = 3, v4 = 4;
    struct linked_list_node *node = malloc(sizeof(*node));
    struct linked_list_node *node2 = malloc(sizeof(*node2));
    struct linked_list_node *node3 = malloc(sizeof(*node3));
    struct linked_list_node *node4 = malloc(sizeof(*node4));

    node->data = &v1;
    node2->data = &v2;
    node3->data = &v3;
    node4->data = &v4;

    node->next = node2;
    node2->next = node3;
    node3->next = node4;
    node4->next = NULL;

    struct linked_list_node *reversed = linked_list_reverse(node);
    assert(*((int*)reversed->data) == 4);
    assert(*((int*)reversed->next->data) == 3);
    assert(*((int*)reversed->next->next->data) == 2);
    assert(*((int*)reversed->next->next->next->data) == 1);
    assert(reversed->next->next->next->next == NULL);
}

void test_has_loop() 
{
    int v1 = 1, v2 = 2, v3 = 3, v4 = 4;
    struct linked_list_node *node = malloc(sizeof(*node));
    struct linked_list_node *node2 = malloc(sizeof(*node2));
    struct linked_list_node *node3 = malloc(sizeof(*node3));
    struct linked_list_node *node4 = malloc(sizeof(*node4));

    node->data = &v1;
    node2->data = &v2;
    node3->data = &v3;
    node4->data = &v4;

    node->next = node2;
    node2->next = node3;
    node3->next = node4;
    node4->next = node;

    assert(linked_list_has_loop(node) == 1);
    node4->next = NULL;
    assert(linked_list_has_loop(node) == 0);
}

int main() 
{
    test_kth_node_from_end();
    test_reverse();
    test_has_loop();
    return 0;
}
